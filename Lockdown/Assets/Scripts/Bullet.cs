﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour 
{
    void OnCollisionEnter(Collision collision)
    {
        GameObject hit = collision.gameObject;
        Movement hitPlayer = hit.GetComponent<Movement>();

        if (hitPlayer != null)
        {
            Combat combat = hit.GetComponent<Combat>();
            combat.TakeDamage(10);

            Destroy(gameObject);
        }
    }
}