﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;
using UnityEngine.UI;

public class CustomNetworkManager : NetworkManager
{
    private const int port = 7777;

    public void StartUpHost()
    {
        if (!NetworkClient.active && !NetworkServer.active)
        {
            NetworkManager.singleton.networkPort = port;
            NetworkManager.singleton.StartHost();
        }
    }

    public void JoinGame()
    {
        if (!NetworkClient.active && !NetworkServer.active)
        {
            NetworkManager.singleton.networkAddress = GameObject.Find("IPAdressTextBox").transform.FindChild("Text").GetComponent<Text>().text;
            NetworkManager.singleton.networkPort = port;
            NetworkManager.singleton.StartClient();
        }
    }

    public void OnLevelWasLoaded(int level)
    {
        if (level == 0)
        {
            GameObject.Find("StartHostButton").GetComponent<Button>().onClick.RemoveAllListeners();
            GameObject.Find("StartHostButton").GetComponent<Button>().onClick.AddListener(StartUpHost);

            GameObject.Find("JoinGameButton").GetComponent<Button>().onClick.RemoveAllListeners();
            GameObject.Find("JoinGameButton").GetComponent<Button>().onClick.AddListener(JoinGame);
        }
        else
        {
            GameObject.Find("DisconnectButton").GetComponent<Button>().onClick.RemoveAllListeners();
            GameObject.Find("DisconnectButton").GetComponent<Button>().onClick.AddListener(NetworkManager.singleton.StopHost);
        }
    }
}