﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using UnityEngine.UI;

public class Player : NetworkBehaviour
{
    public GameObject bulletPrefab;
    public GameObject arm;
    private GameObject camera;

    /* Movement */
    private float speedFactor = 1;

    /* Rotation */
    public float mouseSensitivity = 2;
    private float rotationX, rotationY;

    public override void OnStartLocalPlayer()
    {
        //Set camera
        camera = GetComponentInChildren<Camera>().gameObject;

        //Enable camera and audiolistener for this local player
        GetComponentInChildren<Camera>().enabled = true;
        GetComponentInChildren<AudioListener>().enabled = true;
        arm.GetComponent<MeshRenderer>().enabled = true;

        Cursor.visible = false;

        transform.Translate(new Vector3(0, 1.5f, 0));

        GameObject.Find("Button_JoinRobbers").GetComponent<Button>().onClick.AddListener(JoinRobbers);
        GameObject.Find("Button_JoinHostages").GetComponent<Button>().onClick.AddListener(JoinHostages);
    }

    public void Update()
    {
        //Only update the local player
        if (!isLocalPlayer)
            return;

        //Movement
        Vector3 direction = Vector3.zero;

        direction += Vector3.Cross(Vector3.up, -camera.transform.right) * Input.GetAxis("Vertical");
        direction += camera.transform.right * Input.GetAxis("Horizontal");

        transform.position += Vector3.Normalize(direction) * 0.1f * speedFactor;

        //Rotation
        if (!Cursor.visible)
        {
            rotationX += Input.GetAxis("Mouse X") * mouseSensitivity;

            transform.rotation = Quaternion.AngleAxis(rotationX, Vector3.up);
            camera.transform.rotation = Quaternion.AngleAxis(rotationX, Vector3.up);

            rotationY += Input.GetAxis("Mouse Y") * -mouseSensitivity;
            rotationY = Mathf.Clamp(rotationY, -80, 80);

            camera.transform.rotation *= Quaternion.AngleAxis(rotationY, Vector3.right);
        }

        //Crouching
        if (Input.GetKey(KeyCode.LeftShift))
        {
            speedFactor = Mathf.Lerp(speedFactor, 0.4f, 10 * Time.deltaTime);

            CapsuleCollider capsule = GetComponent<CapsuleCollider>();
            capsule.center = new Vector3(0, -0.4f, 0);
            capsule.height = 1.1f;

            camera.transform.localPosition = Vector3.Lerp(camera.transform.localPosition, new Vector3(0, 0, 0), 10 * Time.deltaTime);
        }
        else
        {
            speedFactor = Mathf.Lerp(speedFactor, 1, 10 * Time.deltaTime);

            CapsuleCollider capsule = GetComponent<CapsuleCollider>();
            capsule.center = new Vector3(0, 0, 0);
            capsule.height = 2f;

            camera.transform.localPosition = Vector3.Lerp(camera.transform.localPosition, new Vector3(0, 0.5f, 0), 10 * Time.deltaTime);
        }

        //Shooting
        if (Input.GetKeyDown(KeyCode.Space))
            CmdFire();

        /*
         * temporary
         */
        if (Input.GetKeyUp(KeyCode.Escape))
            Cursor.visible = !Cursor.visible;
    }

    public void JoinRobbers()
    {
        GameObject.Find("Button_JoinRobbers").SetActive(false);
        GameObject.Find("Button_JoinHostages").SetActive(false);

        tag = "Robber";
    }

    public void JoinHostages()
    {
        GameObject.Find("Button_JoinRobbers").SetActive(false);
        GameObject.Find("Button_JoinHostages").SetActive(false);

        tag = "Hostage";
    }

    [Command]
    void CmdFire()
    {
        GameObject bullet = (GameObject)Instantiate(bulletPrefab, transform.position - transform.forward, Quaternion.identity);

        bullet.GetComponent<Rigidbody>().velocity = -transform.forward * 4;

        NetworkServer.Spawn(bullet);

        Destroy(bullet, 2);
    }
}