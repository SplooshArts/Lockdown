﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Progress : MonoBehaviour {

    Slider slider;
    int i;

    void Start()
    {
        slider = GameObject.Find("ProgressBar").GetComponent<Slider>();
        i = 0;
    }

    public void ChangeValue(int change)
    {
        slider.value += change;
    }

    void Update()
    {
        if (i == 100)
        {
            i = 0;
            ChangeValue(-1);
        }
        else
            i++;
	}
}
